/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/
#include "MuonCondCool/RpcReadWriteCoolStr.h"
#include "MuonCondCool/CscReadWriteCoolStr.h"
#include "MuonCondCool/CscCoolTest.h"
#include "MuonCondCool/TriggerCool.h"
using namespace MuonCalib;

DECLARE_COMPONENT( RpcReadWriteCoolStr )
DECLARE_COMPONENT( CscReadWriteCoolStr )
DECLARE_COMPONENT( CscCoolTest )
DECLARE_COMPONENT( TriggerCool )

